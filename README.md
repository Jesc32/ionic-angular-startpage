# Ionic Angular Startpage

![](https://gitlab.com/Jesc32/ionic-angular-startpage/-/raw/main/showcase.gif)

A custom made home page built using the Ionic Framework with Angular. I created this project as a way to showcase my frontend skills and replace my old home page with one that allows for more customizability.

## Features

- Search Bar
  - [Duckduckgo](https://duckduckgo.com/) search query
  - Bookmark query
- Collapsable Bookmark Menu
  - Sorted into groups featuring [ionicons](https://ionic.io/ionicons)
  - Relative bookmark links supported (i.e. links using the same domain that the page is being hosted from)
- Customizability
  - Name
  - Disable bookmark query
  - Background image/color
  - Primary and text colors
  - Content Alignment, Width, and Opacity

## Defining Bookmarks

As of right now, the bookmarks are defined in the [user service](https://gitlab.com/Jesc32/ionic-angular-startpage/-/blob/main/src/app/services/user.service.ts). Refer to [bookmark](https://gitlab.com/Jesc32/ionic-angular-startpage/-/blob/main/src/app/interfaces/bookmark.ts) and [bookmark group](https://gitlab.com/Jesc32/ionic-angular-startpage/-/blob/main/src/app/interfaces/bookmark-group.ts) interfaces for proper structure/definition. This should be changed down the line, but I still need to decide on the best way to approach this.
