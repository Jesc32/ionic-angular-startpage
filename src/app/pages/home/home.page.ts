import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {
  CheckboxChangeEventDetail,
  InputChangeEventDetail,
  IonModal,
  RangeChangeEventDetail,
  SelectChangeEventDetail
} from '@ionic/angular';
import { cloneDeep } from 'lodash';
import { DynamicSearchComponent } from 'src/app/components/dynamic-search/dynamic-search.component';
import { BookmarkGroup } from 'src/app/interfaces/bookmark-group';

import { Settings } from 'src/app/models/settings';
import { StorageService } from 'src/app/services/storage.service';
import { UserService } from 'src/app/services/user.service';
import { PhotoService, UserPhoto } from 'src/app/services/photo.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {
  @ViewChild('settingsModal') settingsModal: IonModal;
  @ViewChild(DynamicSearchComponent) searchbar: DynamicSearchComponent;

  _form: FormGroup; //Cached settings
  form: FormGroup;
  showBookmarkGroups: boolean;
  loading: boolean = true;

  showBackgroundColorPicker: boolean = false;
  showPrimaryColorPicker: boolean = false;
  showTextColorPicker: boolean = false;

  bookmarkList: BookmarkGroup[];
  photoSub: Subscription;

  photos: UserPhoto[];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private fb: FormBuilder,
    private renderer: Renderer2,
    private storage: StorageService,
    private userService: UserService,
    private photoService: PhotoService
  ) {}

  async ngOnInit() {
    this.bookmarkList = this.userService.fetchBookmarkList();
    this.photoService.loadSaved();

    this.photoSub = this.photoService._photos.subscribe(photos => {
      if (photos) {
        this.photos = photos;
        if (this.loading === false) {
          this.applySettings();
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.photoSub?.unsubscribe();
  }

  ionViewWillEnter() {
    this.initSettings().then(settings => {
      this.form = this.initForm(settings);
      this.formToCache();
      this.applySettings();
    }).finally(() => {
      this.loading = false;
      // TO DO: Shouldn't rely on timeouts this much...
      setTimeout(() => {
        this.searchbar.setFocus();
      }, 100);
    });
  }

  fetchSettingsFromStorage() {
    return Promise.all([
      this.storage.get('username'),
      this.storage.get('bg-img'),
      this.storage.get('bg-url'),
      this.storage.get('bg-color'),
      this.storage.get('primary-color'),
      this.storage.get('text-color'),
      this.storage.get('alignment'),
      this.storage.get('width'),
      this.storage.get('opacity'),
      this.storage.get('show-bookmark-groups'),
      this.storage.get('bookmark-search')
    ]);
  }

  async initSettings(): Promise<Settings> {
    const settings = new Settings();
    let [username, bgImg, bgURL, bgColor, primaryColor, textColor, alignment, width,
        opacity, showBookmarkGroups, bookmarkSearch] = await this.fetchSettingsFromStorage();

    settings.username = username ? username : settings.username;
    settings.backgroundImage = bgImg ? bgImg : settings.backgroundImage;
    settings.backgroundURL = bgURL ? bgURL : settings.backgroundURL;
    settings.backgroundColor = bgColor ? bgColor : settings.backgroundColor;
    settings.primaryColor = primaryColor ? primaryColor : settings.primaryColor;
    settings.textColor = textColor ? textColor : settings.textColor;
    settings.alignment = alignment ? alignment : settings.alignment;
    settings.width = width ? width : settings.width;
    settings.opacity = opacity ? opacity : settings.opacity;
    settings.searchWithBookmarks = bookmarkSearch ? bookmarkSearch : settings.searchWithBookmarks;

    this.showBookmarkGroups = showBookmarkGroups;

    return settings;
  }

  initForm(settings: Settings): FormGroup {
    return this.fb.group({
      username: new FormControl(settings.username, Validators.required),
      backgroundImage: new FormControl(settings.backgroundImage, Validators.required),
      backgroundColor: new FormControl(settings.backgroundColor),
      backgroundURL: new FormControl(settings.backgroundURL),
      primaryColor: new FormControl(settings.primaryColor, Validators.required),
      textColor: new FormControl(settings.textColor, Validators.required),
      alignment: new FormControl(settings.alignment, Validators.required),
      width: new FormControl(settings.width, Validators.required),
      opacity: new FormControl(settings.opacity, Validators.required),
      bookmarkSearch: new FormControl(settings.searchWithBookmarks)
    });
  }

  clearURL() {
    this.form.patchValue({ backgroundURL: null });
  }

  formToCache() {
    this._form = cloneDeep(this.form);
  }

  formFromCache() {
    this.form = cloneDeep(this._form);
  }


  /* General settings methods */

  storeCurrentSettings() {
    const settings: Settings = this.form.getRawValue();
    Promise.all([
      this.storage.set('username', settings.username),
      this.storage.set('bg-img', settings.backgroundImage),
      this.storage.set('bg-url', settings.backgroundURL),
      this.storage.set('bg-color', settings.backgroundColor),
      this.storage.set('primary-color', settings.primaryColor),
      this.storage.set('text-color', settings.textColor),
      this.storage.set('alignment', settings.alignment),
      this.storage.set('width', settings.width),
      this.storage.set('opacity', settings.opacity),
      this.storage.set('bookmark-search', settings.searchWithBookmarks)
    ]).then(_ => {
      this.formToCache();
      this.settingsModal.dismiss();
      this.form.markAsPristine();
    });
  }

  applySettings() {
    const settings: Settings = this.form.getRawValue();
    if (settings.backgroundImage === 'color') {
      this.setDynamicVariable(settings.backgroundColor, '--dynamic-background-color');
    } else if (settings.backgroundImage === 'link') {
      this.setBackgroundImage(settings.backgroundURL);
    } else if (settings.backgroundImage === 'file' && this.photos?.length > 0) {
      this.handleGallerySelection(this.photos[0]);
    }
    this.setDynamicVariable(settings.primaryColor, '--dynamic-primary-color');
    this.setDynamicVariable(settings.textColor, '--dynamic-text-color');
    this.setAlignment(settings.alignment);
    this.setWidth(settings.width);
    this.setDynamicVariable(`${settings.opacity}%`, '--dynamic-opacity');
  }

  // TO DO: add cache to reset photo changes where necessary
  async uploadImage() {
    // TO DO: Display loader until complete
    await this.photoService.uploadExistingImage();
    this.form.markAsDirty();
  }

  removeImage(photo: UserPhoto) {
    this.photoService.removeExistingImage(photo.filepath);
  }


  /* Handler methods for settings modal */

  handleModalWillDismiss() {
    this.formFromCache();

    this.showBackgroundColorPicker = false;
    this.showPrimaryColorPicker = false;
    this.showTextColorPicker = false;

    this.applySettings();
  }

  handleBackgroundTypeChange(event: SelectChangeEventDetail) {
    if (event.value === 'link' || event.value === 'color') {
      this.clearURL();
    }
  }

  handleBackgroundImageToggle(event: CheckboxChangeEventDetail) {
    if (event.checked === false) {
      const color = this.form.get('backgroundColor').value;
      this.handleColorSelect('backgroundColor', color);
    }
  }

  handleBackgroundImageChange(event: InputChangeEventDetail) {
    if (event.value !== "") {
      this.setBackgroundImage(event.value);
    }
  }

  handleColorSelect(controlName: string, color: string) {
    this.form.patchValue({[controlName]: color});
    if (controlName === 'backgroundColor') {
      this.setDynamicVariable(color, '--dynamic-background-color');
    } else if (controlName === 'primaryColor') {
      this.setDynamicVariable(color, '--dynamic-primary-color');
    } else if (controlName === 'textColor') {
      this.setDynamicVariable(color, '--dynamic-text-color');
    }
  }

  handleAlignmentChange(event: SelectChangeEventDetail) {
    this.setAlignment(event.value);
  }

  handleWidthChange(event: RangeChangeEventDetail) {
    this.setWidth(event.value);
  }

  handleOpacityChange(event: RangeChangeEventDetail) {
    this.setDynamicVariable(`${event.value}%`, '--dynamic-opacity');
  }

  handleGallerySelection(photo: UserPhoto) {
    this.form.patchValue({ backgroundURL: photo.webviewPath });
    this.setBackgroundImage(photo.webviewPath);
  }


  /* Setter methods for settings modal */

  setDynamicVariable(value: string, propertyName: string) {
    this.document.documentElement.style.setProperty(propertyName, value);
  }

  setBackgroundImage(url) {
    if (url === null) {
      return;
    }
    this.renderer.setStyle(document.getElementById('bg-image'), 'background-image', `url("${url}")`);
  }

  setAlignment(alignment) {
    this.renderer.setStyle(document.getElementById('main-row'), 'justify-content', alignment);
  }

  setWidth(width) {
    this.renderer.setAttribute(document.getElementById('main-column'), 'size', width);
  }

}
