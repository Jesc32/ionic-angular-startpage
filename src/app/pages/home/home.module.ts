import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { ColorPickerModule } from 'ngx-color-picker';

import { DynamicSearchComponent } from 'src/app/components/dynamic-search/dynamic-search.component';
import { BookmarkAccordionComponent } from 'src/app/components/bookmark-accordion/bookmark-accordion.component';
import { HomePageRoutingModule } from './home-routing.module';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    ColorPickerModule,
    HomePageRoutingModule
  ],
  exports: [
    DynamicSearchComponent,
    BookmarkAccordionComponent
  ],
  declarations: [
    DynamicSearchComponent,
    BookmarkAccordionComponent,
    HomePage
  ]
})
export class HomePageModule {}
