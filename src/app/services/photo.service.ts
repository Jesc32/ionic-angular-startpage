import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Preferences } from '@capacitor/preferences';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  private photos: UserPhoto[] = [];
  public _photos = new BehaviorSubject<UserPhoto[]>(null);
  private PHOTO_STORAGE: string = 'photos';

  constructor() { }

  private async savePicture(photo: Photo): Promise<UserPhoto> {
    // Convert photo to base64 format, required by Filesystem API to save
    const base64Data = await this.readAsBase64(photo);

    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });

    // Use webPath to display the new image instead of base64 since it's
    // already loaded into memory
    return {
      filepath: fileName,
      webviewPath: photo.webPath
    };
  }

  private async updatePreferences() {
    const oldPhotos = await Preferences.get({key: this.PHOTO_STORAGE});
    if (oldPhotos !== null) {
      await Preferences.remove({key: this.PHOTO_STORAGE});
    }
    await Preferences.set({
      key: this.PHOTO_STORAGE,
      value: JSON.stringify(this.photos),
    });
  }

  private async readAsBase64(photo: Photo) {
    // Fetch the photo, read as a blob, then convert to base64 format
    const response = await fetch(photo.webPath!);
    const blob = await response.blob();
  
    return await this.convertBlobToBase64(blob) as string;
  }
  
  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });

  public async uploadExistingImage(): Promise<UserPhoto> {
    // TO DO: Error handling
    const image = await Camera.getPhoto({
      source: CameraSource.Photos,
      resultType: CameraResultType.Uri
    });

    const savedImage = await this.savePicture(image);
    this.photos = [savedImage];
    this._photos.next(this.photos);
    this.updatePreferences();

    return savedImage;
  }

  public removeExistingImage(filepath: string) {
    // TO DO: error handling
    this.photos = this.photos.filter(x => x.filepath !== filepath);
    this._photos.next(this.photos);

    this.updatePreferences();
  }

  public async loadSaved() {
    // Retrieve cached photo array data
    const photoList = await Preferences.get({ key: this.PHOTO_STORAGE });
    this.photos = JSON.parse(photoList.value) || [];
  
    for (let photo of this.photos) {
      // Read each saved photo's data from the Filesystem
      const readFile = await Filesystem.readFile({
        path: photo.filepath,
        directory: Directory.Data,
      });
    
      // Web platform only: Load the photo as base64 data
      photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`;
    }

    this._photos.next(this.photos);
  }
}

export interface UserPhoto {
  filepath: string;
  webviewPath: string;
}