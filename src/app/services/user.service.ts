import { Injectable } from '@angular/core';
import { BookmarkGroup } from '../interfaces/bookmark-group';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  public fetchBookmarkList(): BookmarkGroup[] {
    // TO DO: decide later how I want to store/fetch bookmarks
    // hardcoded for now but may set up a simple DB to handle this
    return [
      {
        title: 'Test 1',
        icon: 'albums-outline',
        bookmarks: [
          { title: 'Test 1.1', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 1.2', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 1.3', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 1.4', link: 'https://gitlab.com/Jesc32'},
        ]
      },
      {
        title: 'Test 2',
        icon: 'bag-outline',
        bookmarks: [
          { title: 'Test 2.1', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 2.2', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 2.3', link: 'https://gitlab.com/Jesc32'},
        ]
      },
      {
        title: 'Test 3',
        icon: 'newspaper-outline',
        bookmarks: [
          { title: 'Test 3.1', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 3.2', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 3.3', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 3.4', link: 'https://gitlab.com/Jesc32'},
          { title: 'Test 3.5', link: 'https://gitlab.com/Jesc32'},
        ]
      }
    ]
  }
}
