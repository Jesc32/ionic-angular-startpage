import { Bookmark } from './bookmark';

export interface BookmarkGroup {
    title: string;          // Bookmark Group Header
    icon?: string;          // Ion icon name, see: https://ionic.io/ionicons
    bookmarks: Bookmark[];  // List of bookmarks to be included in this group
}