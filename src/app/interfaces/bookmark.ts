export interface Bookmark {
    title: string;                  // Bookmark name
    link: string;                   // Bookmark link
    useServerHostname?: boolean;    // Use to link other sites hosted by this server
}
