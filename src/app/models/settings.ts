export class Settings {
    constructor() {
        this.username = 'Guest';
        this.backgroundImage = 'color';
        this.backgroundColor = '#000000';
        this.primaryColor = '#1e1f43';
        this.textColor = '#ffffff';
        this.alignment = 'center';
        this.width = 12;
        this.opacity = 100;
        this.searchWithBookmarks = true;
    }

    searchWithBookmarks: boolean;
    backgroundImage: 'color' | 'file' | 'link';

    username: string;
    backgroundURL?: string;
    backgroundColor: string;
    primaryColor: string;
    textColor: string;
    alignment: 'start' | 'center' | 'end';

    width: number;
    opacity: number;
}
