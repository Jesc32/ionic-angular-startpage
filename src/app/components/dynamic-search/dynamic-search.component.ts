import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonSearchbar, SearchbarChangeEventDetail } from '@ionic/angular';
import { flatMap } from 'lodash';
import { Bookmark } from 'src/app/interfaces/bookmark';
import { BookmarkGroup } from 'src/app/interfaces/bookmark-group';

@Component({
  selector: 'app-dynamic-search',
  templateUrl: './dynamic-search.component.html',
  styleUrls: ['./dynamic-search.component.scss'],
})
export class DynamicSearchComponent implements OnInit {

  @ViewChild(IonSearchbar) searchbar: IonSearchbar;
  @Input() disableSearch: boolean;
  @Input() bookmarkList: BookmarkGroup[];

  allBookmarks: Bookmark[];
  results: Bookmark[];
  selected: Bookmark;
  selectionIndex: number = null;
  searchURL = 'https://duckduckgo.com/?q=';

  location: Location;

  constructor() {}

  ngOnInit() {
    this.allBookmarks = flatMap(this.bookmarkList.map(group => group.bookmarks));
    this.location = window.location;
  }

  setFocus() {
    this.searchbar.setFocus();
  }

  handleInputChange(change: SearchbarChangeEventDetail) {
    if (this.disableSearch === true) {
      return;
    }

    this.selectionIndex = null;
    const search = change.value.toLowerCase();
    if (search.length === 0) {
      this.results = [];
      return;
    }
    this.results = this.allBookmarks.filter(bookmark => bookmark.title.toLowerCase().includes(search));
  }

  handleSelectionChange(event: any) {
    if (this.disableSearch === true) {
      return;
    }

    event.preventDefault(); // Prevents cursor from resetting in text area
    const increment = event.key === "ArrowDown" || event.key === "Tab" ? 1 : -1;
    if (this.results.length === 0) {
      this.selectionIndex = null;
    } else {
      if (this.selectionIndex === null) {
        this.selectionIndex = increment === 1 ? 0 : this.results.length - 1;
      } else if (this.selectionIndex + increment >= this.results.length) {
        this.selectionIndex = 0;
      } else if (this.selectionIndex + increment < 0) {
        this.selectionIndex = this.results.length - 1;
      } else {
        this.selectionIndex += increment;
      }
      this.selected = this.results[this.selectionIndex];
    }
  }

  handleSearch(query: string) {
    if (this.selectionIndex == null) {
      window.location.href = this.searchURL + encodeURIComponent(query);
    } else {
      const bookmark = this.results[this.selectionIndex];
      window.location.href = bookmark.useServerHostname ? 
        location.protocol + '//' + location.hostname + bookmark.link : bookmark.link;
    }
  }

  onEscape() {
    this.selectionIndex === null ? this.searchbar.value = null : this.selectionIndex = null;
  }

}
