import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonAccordionGroup } from '@ionic/angular';
import { BookmarkGroup } from 'src/app/interfaces/bookmark-group';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-bookmark-accordion',
  templateUrl: './bookmark-accordion.component.html',
  styleUrls: ['./bookmark-accordion.component.scss'],
})
export class BookmarkAccordionComponent implements OnInit {

  @ViewChild('bookmarkAccordion') bookmarkAccordion: IonAccordionGroup;
  @Input() showBookmarkGroups: boolean = true;
  @Input() bookmarkList: BookmarkGroup[];

  location: Location;

  constructor(private storage: StorageService) { }

  ngOnInit() {
    this.location = window.location;
  }

  handleMenuToggle() {
    this.showBookmarkGroups = !this.showBookmarkGroups;
    this.storage.set('show-bookmark-groups', this.showBookmarkGroups);
  }

  async handleBookmarkExpandToggle() {
    if (this.showBookmarkGroups !== true) {
      this.showBookmarkGroups = true;
      // TO DO: Find an alternative way to do this
      setTimeout(() => {
        this.bookmarkAccordion.value = this.bookmarkList.map(group => group.title);
      }, 100);
      return;
    }

    if (this.bookmarkAccordion.value == null) {
      this.bookmarkAccordion.value = this.bookmarkList.map(group => group.title);
    } else {
      this.bookmarkAccordion.value = null;
    }
  }

}
